(function ($) {
  Drupal.behaviors.ucCartAjax = {
    attach: function (context, settings) {
      var quantityChangedTimeout;

      $('.qty .form-type-uc-quantity input', context).once('uc-cart-ajax', function () {
        var $input = $(this);

        // Ajaxify quantity inputs
        $input.keypress(function () {
          clearTimeout(quantityChangedTimeout);
          quantityChangedTimeout = setTimeout(function () {
            $input.parents('form').find('input[name="update-cart"]').click();
          }, 500);
        });

        // Quantity spinner
        $input.attr('readonly', true);
        $input.spinner({
          min: 1,
          stop: function(event, ui) {
            $(event.target).keypress();
            $(event.target).change();
          }
        })
      });
    }
  };
}(jQuery));
